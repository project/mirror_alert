<?php

/**
 * @file
 * Inform site owners of domain mirroring of their site.
 */

// Permission that controls access/messaging.
define('MIRROR_ALERT_PERM', 'access administration pages');
// Abstracted page_cache source.
define('MIRROR_ALERT_PAGE_CACHE_TBL', 'cache_page');

/**
 * Operations for every page load.
 */
function mirror_alert_init() {

  // Inform everywhere if there are mirrors in the page cache.
  global $user;
  if (user_access(MIRROR_ALERT_PERM, $user)) {
    // Look for mirror alert notices.
    $result = db_query(
      'SELECT count(wid) as num FROM {watchdog}
      WHERE type = "mirror_alert"');
    $row = db_fetch_array($result);
    if ($row['num'] != 0) {
      drupal_set_message(t('Found at least one mirrored
        site notice in your !log.', array('!log' => l(t('log entries'), 'admin/reports/dblog'))), 'warning');
    }
  }

  // Check for use of base URL.
  if (variable_get('mirror_alert_authusercheck', 0)) {
    // Method borrowed directly from conf_init().
    $active_root = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
    $active_url = $active_root .= '://' . $_SERVER['HTTP_HOST'];
    // Non-base URL site detected.
    global $base_url;
    if ($active_url != $base_url) {
      $msg = t('Caught this non-base domain (@active) site in your page cache at: @page');
      $vars = array(
        '@active' => $active_url,
        '@page' => $active_url . '/' . $_SERVER['REQUEST_URI']);
      watchdog('mirror_alert', $msg, $vars, WATCHDOG_WATCHDOG_ALERT);
    }
  }

}

/**
 * Register reporting and settings page.
 */
function mirror_alert_menu() {
  $items = array();
  $items['admin/reports/mirror-alert'] = array(
    'title'             => t('Mirror Alert Report'),
    'description'       => t('Be aware of what domains are accessing your site and serving your content!'),
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('mirror_alert_page'),
    'access arguments'  => array('access administration pages'),
    'type'              => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Build reporting and settings page.
 */
function mirror_alert_page() {

  $form = array(
    'mirror_alert_intro' => array(
      '#value' => t('<p>This module will check for alternate (non-base URL) domains in your page cache.</p>
        <p>In the event that other domains are found, this does not mean you\'ve been hacked.
        Please read the <a href="http://drupal.org/project/mirror_alert">project page</a>
        for "What do to with results."</p>
        <p>This module has two modes which will depend on if you\'ve set $base_url in your
        sites/default/settings.php file. See the project page for details.</p>
        <p>NOTE: This module is not currently integrated with modules like Domain Access and others
        that allow normal multi-domain behavior.</p>'),
    ),
    'mirror_alert_authusercheck' => array(
      '#type' => 'checkbox',
      '#title' => t('Check for mirror on page loads.'),
      '#default_value' => variable_get('mirror_alert_authusercheck', 0),
      '#description' => t('Will perform a light-weight check for current domain
        vs base url on <u>non-cached</u> page loads.<br >NOTE: This is the
        <u>only way</u> to make use of this module if $base_url is hard coded
        in settings.php'),
    ),
    'mirror_alert_report' => array(
      '#value' => mirror_alert_view_report(),
    ),
  );

  return system_settings_form($form);
}

/**
 * Build more detailed report.
 */
function mirror_alert_view_report() {
  global $base_url;
  // Check if anything other than base_url is in the page cache.
  if (!_mirror_alert_quick_check()) {
    $rows = array();
    $result = db_query(
      'SELECT cid as url, count(*) as num FROM {' . MIRROR_ALERT_PAGE_CACHE_TBL . '}
      WHERE cid NOT LIKE "' . $base_url . '%"
      GROUP BY SUBSTRING_INDEX( SUBSTRING_INDEX(cid,"/",3), ".", 1)');
    while ($row = db_fetch_array($result)) {
      $rows[] = $row;
    }
    $output .= '<h3>' . t('Report from page cache') . '</h3>';
    $output .= theme_table(array('URL', 'Occurances'), $rows);
  }
  else {
    $output .= t('<strong>Everything is fine.</strong> No alternate domains in the page cache.<br /><br />');
  }
  return $output;
}

/**
 * Check for mirrored access within the page cache.
 */
function mirror_alert_cron() {
  if ((variable_get('cache', 0) || variable_get('page_compression', 0))) {
    if (_mirror_alert_quick_check()) {
      $msg = t('Potential mirrored site. Found at least one non-base URL site in your page cache.');
      watchdog('mirror_alert', $msg, NULL, WATCHDOG_ALERT);
    } // else, all is well.
  }
  else {
    // Inform via watchdog on cron the tool is unable to work.
    $msg = t('Finding mirrored site access requires turning on performance cache.');
    watchdog('mirror_alert', $msg, NULL, WATCHDOG_WARNING);
  }
}

/**
 * Are there any rouge page cache entries? (True = all clear.)
 */
function _mirror_alert_quick_check() {
  global $base_url;
  // Check if anything other than base_url is in the page cache.
  $result = db_query(
    'SELECT count(cid) as num FROM {' . MIRROR_ALERT_PAGE_CACHE_TBL . '}
    WHERE cid NOT LIKE "' . $base_url . '%"'
  );
  $row = db_fetch_array($result);
  if (db_affected_rows($result) > 1 || $row['num'] != 0) {
    // Something came up.
    return FALSE;
  }
  else {
    // All clear.
    return TRUE;
  }

}
